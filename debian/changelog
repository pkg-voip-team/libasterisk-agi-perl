libasterisk-agi-perl (1.08-1) unstable; urgency=medium

  * New upstream release
  * Extra build dep: libnet-telnet-perl for tests
  * Add Fix-pod2man-errors.patch
  * Add Typo-in-man-page.patch
  * rules: remove get-orig-source target
  * Update VCS URLs and homepage
  * Standards version 3.9.8 (no change needed)

 -- Tzafrir Cohen <tzafrir@debian.org>  Fri, 23 Dec 2016 01:08:12 +0200

libasterisk-agi-perl (1.03-1) unstable; urgency=medium

  * New upstream release (Closes: #782167):
    debian/patches/001_fix_pod_errors.patch dropped: merged upstream
  * New standards version (3.9.6) - no changed needed
  * Dropped README.source: using standard procedure
  * 4be827e Compat level 9
  * d4414a7 A DEP-5 copyright file
  * 0dcbaa7 Switch to debhelper
  * fc4f3d3 Convert to dpkg V.3 (quilt) format

 -- Tzafrir Cohen <tzafrir@debian.org>  Sat, 22 Aug 2015 02:05:52 +0200

libasterisk-agi-perl (1.01-2) unstable; urgency=low

  * Use DESTDIR instead of PREFIX to avoid ftbfs with Perl 5.10.1
  * set Standards-version to 3.8.3 (no changes needed)

 -- Rene Mayorga <rmayorga@debian.org>  Sun, 13 Sep 2009 19:55:04 -0600

libasterisk-agi-perl (1.01-1) unstable; urgency=low

  [ Kilian Krause ]
  * Remove -N from wget args in get-orig-source target as -O is already
    used.

  [ Tzafrir Cohen ]
  * New upstream release.
  * Fixed watch file.

  [ Rene Mayorga ]
  * Change my email address
  * Use standard-version 3.8.2 (no changes needed)
  * use debian/compat and debhelper version 7
  * debian/rules: 
    + use dh_prep instead of dh_clean -k
    + enable tests 
    + use quilt to manage the patches
  * fix POD errors ( add 001_fix_pod_errors.patch)
  * update debian/copyright: add info about examples/directory.agi
  * add README.Source

 -- Rene Mayorga <rmayorga@debian.org>  Sun, 05 Jul 2009 22:00:28 -0600

libasterisk-agi-perl (0.10-2) unstable; urgency=low

  [ Rene Mayorga ]
  * debian/rules
    + Remove /usr/lib/perl5 only if exists
      (Closes: #467662)
    + remove un-ussed dh_installdirs
  * debian/control
    + Raise standards-version to 3.7.3 ( no changes needed )
    + Set debhelper version to 6
    + Move perl from B-D to B-D-I

  [ Faidon Liambotis ]
  * Add myself to Uploaders.

 -- Faidon Liambotis <paravoid@debian.org>  Thu, 06 Mar 2008 03:07:12 +0200

libasterisk-agi-perl (0.10-1) unstable; urgency=low

  [ Rene Mayorga ]
  * New upstream release
  * [debian/rules]
    + de-ignoring output from $(MAKE) distclean
    + removing debian/$package/usr/lib/perl5 - was empty
    + get-orig-source and print-version targets added
  * [debian/control]
    + Maintainer field set to Debian VoIP Team
    + moved myself to Uploaders
    + ${misc:Depends} added
    + XS-Vcs-(Svn|Browser) field(s) added

  [ Faidon Liambotis ]
  * Removed versioned dependency on perl for a pre-sarge version.

  [ Kilian Krause ]
  * Add Homepage field as added in dpkg-dev 1.14.6.
  * Correct URL in debian/copyright to actual download location.
  * Indent list in Description fixing
    possible-unindented-list-in-extended-description

 -- Kilian Krause <kilian@debian.org>  Sat, 06 Oct 2007 12:11:34 +0200

libasterisk-agi-perl (0.09-1) unstable; urgency=low

  * Initial release (Closes: #424956)

 -- Rene Mayorga <rmayorga@debian.org.sv>  Sat, 19 May 2007 13:08:33 -0600

